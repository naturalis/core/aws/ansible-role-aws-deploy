ansible-role-aws-deploy
=========

This role deploys your host in the hosts file in a specified region

Requirements
------------
Local requirements:
  - boto3: ` sudo pip3 install boto3`
  - botocore: ` sudo pip3 install botocore`
  - environment variable: aws_ec2_deploy_keyname mwith your keypair name `export AWS_DEPLOY_KEYNAME=<aws_keypair_name>` 
  - Ansible amazon.aws collection: `ansible-galaxy collection install amazon.aws:1.5.1 --force` 

Role Defaults
--------------
Security rules:
By default the `default_management security` group will be added to all deployed instances. 

Role Variables
--------------
All role variables are set using this naming convention: `aws_<aws_function>_variable name`


Required inputs:
  - `service: ` - Name of the service to deploy
  - `service_owner: ` - Name ot the owner of a service, usually the contact for the service
  - `service_team: ` - Name of the team responsible for the service
  - `aws_region: eu-west-1|eu-central-1` (Currently only these two regions are available)
  - `aws_iam_deploy_key: ` - AWS IaM user key to deploy. Is obtained from Vault
  - `aws_iam_deploy_secret: ` - AWS IaM user secret to deploy. Is obtained from Vault.
  - `aws_ec2_instance_type: <instance_type>` - Use any AWS instance type name
  - `aws_ec2_deploy_keyname: ` - EC2 deploy key, is obtained from $AWS_DEPLOY_KEYNAME shel variable. See Requirements.
  
Defaults (can be overridden locally):
  - `aws_vpc_name: default-vpc` (Currently no other vpc's exist)
  - `aws_ec2_image_name: ubuntu-20.04` (Currently only ubuntu-20.04 is supported)
  - `aws_ec2_image_version: latest` (build_version are also allowed. Check tags for AMI's in either region)
  - `aws_ec2_root_volume_type: gp2` (Sets root volume type)
  - `aws_ec2_data_volume_device_name: /dev/sda1` (Sets root OS local device name)
  - `aws_ec2_data_volume_delete_on_termination:true` - Delete data volumes when the instance is terminated
  - `aws_ec2_root_volume_size: 8`  (Sets root volume size)
  - `aws_ec2_data_volume_type: gp2` (Set extra data disk volume type, only needed when `aws_ec2_data_volume_size:` is set)
  - `aws_ec2_root_volume_delete_on_termination:true` - Delete root volumes when the instance is terminated

Local variables
  - security rules:

  ```yaml
  aws_security_rules:
  - name: allow-web
    description: allow 80/443 access from any
    protocol: tcp
    ports:
      - 80
      - 443
    from: 0.0.0.0/0
  ```
  - Data volume size
  
  This allows for the creation of a volume to connect with the EC2 instance and is mounted locally under /data
  `aws_ec2_data_volume_size:` - Size of the /data mount in gigabytes

Dependencies
------------

This role uses secrets from hashicorp vault, so make sure your Vault token is valid en have installed the hashicorp vault ansible module

Example Playbook
----------------
- Requirements.yml
```yaml
 - src: https://gitlab.com/naturalis/core/aws/ansible-role-aws-deploy
    scm: git
    version: <current version number>
    name: aws-deploy
```
- Hosts
```ini
[dev]
instance-dev-1 aws_ec2_instance_type=t2.micro aws_ec2_data_volume_size=10

[prod]
instance-prod-1 aws_ec2_instance_type=t2.medium
```

Playbook include after hashicopr and before any other config playbooks
```yaml
- name: Deploy AWS instances
  hosts: all
  tags: always
  gather_facts: false
  tasks:
    - name: AWS deploy
      include_role:
        name: aws-deploy
        apply:
          tags: aws-deploy
```

License
-------

BSD

