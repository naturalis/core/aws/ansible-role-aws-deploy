---
# Input assertions
- name: Assert AWS input variables
  run_once: true
  delegate_to: localhost
  no_log: true
  assert:
    that: item | length > 0
    fail_msg: "{{ item }} option is not set in vars, make sure it is present"
    quiet: true
  with_items:
    - "{{ service }}"
    - "{{ service_owner }}"
    - "{{ service_team }}"
    - "{{ aws_region }}"
    - "{{ aws_iam_deploy_key }}"
    - "{{ aws_iam_deploy_secret }}"
    - "{{ aws_ec2_instance_type }}"
    - "{{ aws_ec2_deploy_keyname }}"
    
# Gather AWS resource ID's
- name: Get VPC id
  delegate_to: localhost
  run_once: true
  import_tasks: vpc.yml
- name: Get AMI image id
  delegate_to: localhost
  run_once: true
  import_tasks: ami.yml
- name: Get subnet id
  delegate_to: localhost
  run_once: true
  import_tasks: subnet.yml

# Create AWS resources
- name: Create EC2 security groups
  delegate_to: localhost
  run_once: true
  import_tasks: security_groups.yml
- name: Create EC2 instances and volumes
  delegate_to: localhost
  import_tasks: ec2.yml

# Update inventory with the IP addresses of the instances
- name: Add instances to Inventory
  delegate_to: localhost
  run_once: true
  add_host:
    name: "{{ item }}"
    groups: "{{ hostvars[item].group_names }}"
    ansible_host: "{{ hostvars[item].ec2.tagged_instances[0].public_ip }}"
  loop: "{{ ansible_play_hosts }}"
  check_mode: false
  changed_when: false

# Make sure the initial deployment is done and the instances are available for configuration
- name: Wait for SSH to come up
  remote_user: ubuntu
  wait_for_connection:
    delay: 10
    timeout: 320
  check_mode: false
  when: ec2.changed

# These steps make sure the user deploying is added during initial deployment of an instance as a root user. 
# Only done when the deploying user is NOT using ubuntu as the default user and only at deployment
- name: set deployment user
  set_fact:
    deployment_user: "{{ ansible_user }}"
  when: 
    - ec2.changed 
    - ansible_user is defined
    - ansible_user != 'ubuntu'

- block:
  - name: Add extra user when current user is not Ubuntu
    become: true
    user:
      name: "{{ deployment_user }}"
      shell: /bin/bash

  - name: Add root access for current user with passwordless sudo
    become: true
    lineinfile:
      dest: /etc/sudoers
      state: present
      regexp: "^%{{ deployment_user }}"
      line: "%{{ deployment_user }} ALL=(ALL) NOPASSWD: ALL"
      validate: 'visudo -cf %s'
  
  - name: Copy file with owner and permissions
    become: true
    copy:
      remote_src: true
      src: "/home/ubuntu/.ssh"
      dest: "/home/{{ deployment_user }}/"
      owner: "{{ deployment_user }}"
      group: "{{ deployment_user }}"
  remote_user: ubuntu
  when: deployment_user is defined

# These 2 steps are done to make sure packages can be installed. A race condition can occur is the update_cache is included and run for the first time when actual packages are also named like in the base role
# Only run on initial deployment
- name: Wait for any possibly running unattended upgrade to finish
  raw: systemd-run --property="After=apt-daily.service apt-daily-upgrade.service" --wait /bin/true
  changed_when: false
  become: true
- name: update apt cache
  apt:
    update_cache: true
    cache_valid_time: 86400
  become: true

# Mount volume when extra disk is present
- name: Mount new volume on /data
  import_tasks: mount.yml
  when: ec2_vol.changed

# Log some default information about the build
- name: Logging new ec2 instances IP
  debug:
    msg:
      - "IP: {{ hostvars[inventory_hostname].ec2.tagged_instances[0].public_ip }}"
- name: Logging new ec2 instances Volumes
  debug:
    msg:
      - "Volume name: {{ ec2_vol.volume.tags.Name }}"
      - "Volume size: {{ ec2_vol.volume.size }} Gb"
      - "Volume type: {{ ec2_vol.volume_type}}"
      - "Volume path : {{ ec2_vol.volume.attachment_set.device }}"
      - "Volume delete on termination: {{ aws_ec2_data_volume_delete_on_termination }}"
  when: ec2_vol.changed
